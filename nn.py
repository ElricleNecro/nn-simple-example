#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Basic neural network based on numpy, for the show.
"""


import numpy as np


def nonlin(x, deriv=False):
    """Sigmoid function.

    Map any value to a value between 0 and 1 to simulate a neuron answer.

    :param x: Value to map
    :param deriv: Should we return the derivative of the function?
    :type deriv: bool

    :return: A value between 0 and 1
    :rettype: float
    """
    if deriv:
        return x * (1 - x)

    return 1 / (1 + np.exp(-x))


if __name__ == "__main__":
    # Input data:
    X = np.array([[0, 0, 1], [0, 1, 1], [1, 0, 1], [1, 1, 1]])

    # Output data:
    y = np.array([[0], [1], [1], [0]])

    # Initialising the seed for the random number generator:
    np.random.seed(1)

    # Synapses matrices, we have 3 layer, and so we need 2 synapses matrices:
    syn0 = 2 * np.random.random((3, 4)) - 1
    syn1 = 2 * np.random.random((4, 1)) - 1

    # Training:
    for j in range(1000000):
        # the first layer is our input:
        l0 = X
        # create second layer by applying the neuron response function:
        l1 = nonlin(np.dot(l0, syn0))
        # create the final layer:
        l2 = nonlin(np.dot(l1, syn1))

        l2_error = y - l2

        if j % 10000 == 0:
            print("Error:", np.mean(np.abs(l2_error)))

        # Backpropagating the errors to reduce the error rate:
        l2_delta = l2_error * nonlin(l2, deriv=True)

        # Getting the errors of the second layer using the errors of the last layer:
        l1_error = l2_delta.dot(syn1.T)
        l1_delta = l1_error * nonlin(l1, deriv=True)

        # Using the Gradient descent to change our synapses matrices to reduce the errors:
        syn1 += l1.T.dot(l2_delta)
        syn0 += l0.T.dot(l1_delta)

    print("After training:", l2)
